#Docker file
FROM tomcat
MAINTAINER "SWAGAT"
RUN apt-get update
WORKDIR /usr/local/tomcat/
COPY tomcat-users.xml /usr/local/tomcat/conf
COPY context.xml /usr/local/tomcat/webapps/manager/META-INF
COPY ./target/*.war /usr/local/tomcat/webapps
EXPOSE 8080
